<?php

/**
 * @dva
 * +- комменты у функций есть, но не по стандарту
 * - лучше обернуть код в класс
 * - код очень сложный для понимания, я бы не смог исправить баги сам в разумное время.
 *   там где preg_match_all лушче давать примеры в комментах типа 'было' => 'стало'.
 * - в коде используется @ - это плохо
 */

/**
 * @param string
 *
 * @return int
 */
function calculator($content)
{
    $regex = '#\(([\w\+\-]*?)\)#';
    $content = str_replace(' ', '', $content);

    while (true) {
        $matchResult = preg_match_all($regex, $content, $matches);

        if ($matchResult == 0) {
            break;
        }

        foreach ($matches[1] as $keyMatch => $val) {
            $equation = calculatorStingParser($val);
            $result = calculatorProcess($equation);

            $content = str_replace($matches[0][$keyMatch], $result, $content);
        }
    }

    $equation = calculatorStingParser($content);
    $result = calculatorProcess($equation);

    return $result;
}

/**
 * @param string
 *
 * @return array
 */
function calculatorStingParser($string)
{
    $equation = array();
    $i = 0;
    $count = 0;

    do {
        $char = $string{$i};

        switch ($char) {
   case '+':
   case '-':
    $equation['char'][$count + 1] = $char;
    $count++;

    break;

   default:
    if (!isset($equation['num'][$count])) {
        $equation['num'][$count] = $char;
    } else {
        $equation['num'][$count] .= $char;
    }

    $equation['char'][$count + 1] = '+';

    break;
  }

        $i++;
    } while (isset($string{$i}));

    return $equation;
}

/**
 * @param array
 *
 * @return int
 */
function calculatorProcess($equation)
{
    $result = 0;

    foreach ($equation['num'] as $key => $num) {
        $char = @$equation['char'][$key];

        switch ($char) {
   case '+':
   default:
    $result += $num;
    break;

   case '-':
    $result -= $num;
    break;
  }
    }

    return $result;
}

///https://drive.google.com/a/mirasvit.com/file/d/0B767swb56E_YZ0N3aThuVk9LalU/edit
function test($expectedResult, $input)
{
    $result = calculator($input);
    if ($expectedResult == $result) {
        echo "OK\n";
    } else {
        echo "FAIL: $input . Expected: $expectedResult. Got: $result. \n";
    }
}

test(-14, '1-5-5-5'); //OK
test(-4, '1-5-(5-5)'); //OK
test(4, '1-5-(5-5-(7-3-(4+3-10)+   (13-12)))'); //FAIL: 1-5-(5-5-(7-3-(4+3-10)+   (13-12))) . Expected: 4. Got: -6.
test(4, '(1 - 3) + (3 + 3)'); //OK
test(9, '-7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3)'); //FAIL: -7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3) . Expected: 9. Got: -29.
