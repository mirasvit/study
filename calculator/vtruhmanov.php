<?php

/**
 * @dva
 * +++ единственный код, который прошел все тесты
 * - лучше если код поместить в класс
 * - нет комментов у функций
 * - код очень сложный для понимания, я бы не смог исправить тут баги
 */
function parse($expr)
{
    $parsedExpr = array();
    $curDigit = 0;
    $parsedExpr[$curDigit] = '';
    for ($i = 0; $i < strlen($expr); $i++) {
        if ($expr[$i] != ' ') { // @dva - можно убрать лишнюю вложенность
            if ($expr[$i] == '(') {
                // Find proper inner parentheses
                $pstack = 1;
                for ($j = $i + 1; $j < strlen($expr); $j++) {
                    if ($expr[$j] == '(') {
                        $pstack++;
                    }
                    if ($expr[$j] == ')') {
                        $pstack--;
                    }
                    if ($pstack == 0) {
                        break;
                    }
                }
                // parse inner expression and assign to new element
                $curDigit++;
                $parsedExpr[$curDigit] = parse(substr($expr, $i + 1, $j - $i - 1));
                $curDigit++;
                $parsedExpr[$curDigit] = '';
                $i = $j;
            } else {
                $parsedExpr[$curDigit] = $parsedExpr[$curDigit].$expr[$i];
                if (isset($expr[$i + 1])) {
                    if (($expr[$i + 1] == '+' || $expr[$i + 1] == '-' || $expr[$i + 1] == ' ' || $expr[$i] == '+' || $expr[$i] == '-')) {
                        $curDigit++;
                        $parsedExpr[$curDigit] = '';
                    }
                }
            }
        }
    }

    return $parsedExpr;
}

function evaluate($exprArray)
{
    $total = 0;
    $currentAction = '+';
    foreach ($exprArray as $element) {
        if ($element == '+' || $element == '-') {
            $currentAction = $element;
        } else {
            $operand = $element;
            if (is_array($operand)) {
                $operand = evaluate($element);
            }
            switch ($currentAction) {
                case '+':
                    $total = $total + $operand;
                    break;
                case '-':
                    $total = $total - $operand;
                    break;
            }
        }
    }

    return $total;
}

function test($expectedResult, $input)
{
    $result = evaluate(parse($input));
    if ($expectedResult == $result) {
        echo "OK\n";
    } else {
        echo "FAIL: $input . Expected: $expectedResult. Got: $result \n";
    }
}

test(-14, '1-5-5-5'); //OK
test(-4, '1-5-(5-5)'); //OK
test(4, '1-5-(5-5-(7-3-(4+3-10)+   (13-12)))'); //OK
test(4, '(1 - 3) + (3 + 3)'); //OK
test(9, '-7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3)'); //OK
