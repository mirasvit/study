<?php

/**
 * @dva
 * - нет комментов у функций
 * - код очень сложный для понимания, я бы не смог исправить баги сам в разумное время.
 *   там где preg_match_all лушче давать примеры в комментах типа 'было' => 'стало'.
 */
class Solution
{
   public function calculate($s)
   {
       $s = str_replace(' ', '', $s);
       while (preg_match_all('/\(([^\(\)]*)\)/', $s, $expressionArray)) {
           foreach ($expressionArray[0] as $value) {
               $valueResult = $this->_getCalculatedResult($value);
               $s = str_replace($value, $valueResult, $s);
               $s = $this->_prepareString($s);
           }
       }

       return $this->_getCalculatedResult($s);
   }

    protected function _getCalculatedResult($s)
    {
        $search = array('(', ')');
        $s = str_replace($search, '', $s);
        preg_match_all('/\d+/', $s, $numbers);
        preg_match_all('/[\+-]/', $s, $operators);
        $result = $numbers[0][0];
        array_shift($numbers[0]);
        foreach ($numbers[0] as $key => $value) {
            $result = ($operators[0][$key] == '+') ? ($result + $value) : ($result - $value);
        }

        return $result;
    }

    protected function _prepareString($s)
    {
        $s = str_replace('--', '+', $s); // нужно для 5-(19-20) /5--1/
       $s = str_replace('+-', '-', $s); // нужно для 5+(19-20) /5+-1/

       return $s;
    }
}

function test($expectedResult, $input)
{
    $solution = new Solution();
    $result = $solution->calculate($input);
    if ($expectedResult == $result) {
        echo "OK\n";
    } else {
        echo "FAIL: $input . Expected: $expectedResult. Got: $result. \n";
    }
}

test(-14, '1-5-5-5'); //OK
test(-4, '1-5-(5-5)'); //OK
test(4, '1-5-(5-5-(7-3-(4+3-10)+   (13-12)))'); //OK
test(4, '(1 - 3) + (3 + 3)'); //FAIL: (1 - 3) + (3 + 3) . Expected: 4. Got: -4.
test(9, '-7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3)'); //FAIL: -7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3) . Expected: 9. Got: -13.
