<?php

/**
 * @dva
 * + код понятный
 */
class Solution
{
    //@dva - нет коммента
    public function calculate($expr)
    {
        $expr = str_replace(array('(', ')', ' '), '', $expr);
        $expArr = str_split($expr);
        $result = 0;
        $action = null;
        foreach ($expArr as $sign) { //@dva - мне кажется что переменную лучше называть не $sign (она ведь может быть числом)
            if (!is_numeric($sign)) { //@dva - обратная логика
                $action = $sign;
            } else {
                if ($action === null) {
                    $result = $sign;
                }
                switch ($action) {
                    case '+':
                        $result += $sign;
                        break;
                    case '-':
                        $result -= $sign;
                        break;
                }
            }
        }

        return $result;
    }
}

function test($expectedResult, $input)
{
    $solution = new Solution();
    $result = $solution->calculate($input);
    if ($expectedResult == $result) {
        echo "OK\n";
    } else {
        echo "FAIL: $input . Expected: $expectedResult. Got: $result \n";
    }
}

test(-14, '1-5-5-5'); //OK
test(-4, '1-5-(5-5)'); //FAIL: 1-5-(5-5) . Expected: -4. Got: -14
test(4, '1-5-(5-5-(7-3-(4+3-10)+   (13-12)))'); //FAIL: 1-5-(5-5-(7-3-(4+3-10)+   (13-12))) . Expected: 4. Got: -25
test(4, '(1 - 3) + (3 + 3)'); //OK
test(9, '-7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3)'); //FAIL: -7-8-(3-(1-(1-5-(7-9)-8))-10) + (3 + 3) . Expected: 9. Got: -44
